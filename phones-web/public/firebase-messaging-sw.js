importScripts("https://www.gstatic.com/firebasejs/8.1.2/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.1.2/firebase-messaging.js");

const config = {
  apiKey: "AIzaSyCnXNqLj5_KUzoitPy9WK1yopyU0kfVCbU",
  authDomain: "soa-phones.firebaseapp.com",
  projectId: "soa-phones",
  storageBucket: "soa-phones.appspot.com",
  messagingSenderId: "912540496355",
  appId: "1:912540496355:web:50b3db36cab6adc71df0d6",
};

firebase.initializeApp(config);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
  console.log(
    "[firebase-messaging-sw.js] Received background message ",
    payload
  );
  const notificationTitle = payload.data.title;
  const notificationOptions = {
    body: payload.data.body,
  };
  return self.registration.showNotification(
    notificationTitle,
    notificationOptions
  );
});

self.addEventListener("notificationclick", (event) => {
  console.log(event);
  return event;
});
