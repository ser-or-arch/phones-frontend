import React, { Fragment } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import LoginForm from "./components/LoginForm.js";
import PhonesPage from "./components/PhonesPage.js";
import "./styles/index.css";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      window.sessionStorage.getItem("state") === "logged" ? (
        <Component {...props} />
      ) : (
        <Redirect to="/login" />
      )
    }
  />
);

export default class App extends React.Component {
  render() {
    return (
      <Fragment>
        <ToastContainer autoClose={2000} position="top-center" />
        <Container className="center-column">
          <Row>
            <Col>
              <BrowserRouter>
                <Switch>
                  <Route path="/login" component={LoginForm} />
                  <PrivateRoute path="/" component={PhonesPage} />
                </Switch>
              </BrowserRouter>{" "}
            </Col>
          </Row>
        </Container>
      </Fragment>
    );
  }
}
