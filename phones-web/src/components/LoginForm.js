import React, { Component } from "react";
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import { login, saveToken } from "../services/LoginService";
import "../styles/login.css";
import { requestFirebaseNotificationPermission } from "../firebaseInit";

export default class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange = (event) => {
    this.setState({
      [event.target.id]: event.target.value,
    });
  };

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const { username, password } = this.state;

    if (!(username && password)) {
      return;
    }

    login(username, password, this);
  }

  render() {
    if (
      window.sessionStorage.getItem("state") === "logged" &&
      this.state.submitted
    ) {
      requestFirebaseNotificationPermission()
        .then((firebaseToken) => {
          saveToken(window.sessionStorage.getItem("username"), firebaseToken);
        })
        .catch((err) => {
          alert(err);
          return err;
        });
      return <Redirect to="/" />;
    }

    return (
      <div className="login_div">
        <div className="login_box">
          <div className="login_content_div">
            <form id="login_form" onSubmit={this.handleSubmit}>
              <FormLabel id="login_title">Login</FormLabel>

              <FormGroup
                className="form-group"
                controlId="username"
                bsSize="large"
              >
                <FormLabel id="login_lable">Username: </FormLabel>
                <FormControl
                  autoFocus
                  type="username"
                  value={this.state.username}
                  onChange={this.handleChange}
                />
              </FormGroup>
              {this.state.submitted && !this.state.username && (
                <div>Username is required!</div>
              )}
              <FormGroup
                className="form-group"
                controlId="password"
                bsSize="large"
              >
                <FormLabel id="login_lable">Password: </FormLabel>
                <FormControl
                  value={this.state.password}
                  onChange={this.handleChange}
                  type="password"
                />
              </FormGroup>
              {this.state.submitted && !this.state.password && (
                <div>Password is required!</div>
              )}
              <Button
                id="login_button"
                bsStyle="default"
                bsSize="large"
                type="submit"
              >
                Login
              </Button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
