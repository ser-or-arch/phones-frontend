import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Button,
  Input,
  Form,
  FormGroup,
} from "reactstrap";
import Popup from "reactjs-popup";
import Table, { Thead, Tbody } from "react-row-select-table";
import { confirmAlert } from "react-confirm-alert";
import {
  getPhones,
  insertPhone,
  deletePhone,
  updatePhone,
} from "../services/PhoneService";
import { priceInRon } from "../services/CurrencyService";
import "../styles/phones.css";
import { onMessageListener } from "../firebaseInit";
import { toast } from "react-toastify";
import { ToastContainer } from "react-toastify";

class PhonesPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      thead: ["ID", "Name", "Description", "Price", "Actions"],
      tbody: [],
      tbodyall: [],
      updateIndex: -1,
      addPopupOpen: false,
      updatePopupOpen: false,
      searchString: "",
    };
    this.onAddBtnClick = this.onAddBtnClick.bind(this);
    this.onDeleteBtnClick = this.onDeleteBtnClick.bind(this);
    this.onUpdateBtnClick = this.onUpdateBtnClick.bind(this);
    this.addNewPhone = this.addNewPhone.bind(this);
    this.updatePhone = this.updatePhone.bind(this);
    this.closeAddPopup = this.closeAddPopup.bind(this);
    this.deletePhone = this.deletePhone.bind(this);
    this.closeUpdatePopup = this.closeUpdatePopup.bind(this);

    this.handleSearch = this.handleSearch.bind(this);
    this.changeTbody = this.changeTbody.bind(this);
  }

  componentDidMount() {
    getPhones().then((response) =>
      this.setState({ tbodyall: response, tbody: response })
    );
  }

  deletePhone(key) {
    const phone = this.state.tbody[key];
    deletePhone(phone).then(() => {
      let phones = this.state.tbody;
      phones.splice(key, 1);
      this.setState({ tbody: phones });
    });
  }

  onDeleteBtnClick(key) {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className="custom-ui">
            <h1>
              Are you sure you want to delete{" "}
              <strong>{this.state.tbody[key].name} </strong>?
            </h1>
            <Button
              onClick={() => {
                this.deletePhone(key);
                onClose();
              }}
            >
              Yes
            </Button>
            <Button onClick={onClose}>No</Button>
          </div>
        );
      },
    });
  }

  onUpdateBtnClick(key) {
    this.setState({ updatePopupOpen: true, updateIndex: key });
  }

  onAddBtnClick() {
    this.setState({ addPopupOpen: true });
  }

  onPriceInRonBtnClick(key) {
    const phone = this.state.tbody[key];
    priceInRon(phone).then((res) => {
      alert(res + " RON");
    });
  }

  addNewPhone(phone) {
    insertPhone(phone).then((response) => {
      let phones = this.state.tbody;
      phones.push(response);
      this.setState({ tbody: phones });
      this.closeAddPopup();
    });
  }

  updatePhone(phone) {
    updatePhone(phone).then((response) => {
      let phones = this.state.tbody;
      phones.splice(this.state.updateIndex, 1, response);
      this.setState({ tbody: phones });
      this.closeUpdatePopup();
    });
  }

  closeAddPopup() {
    this.setState({ addPopupOpen: false });
  }

  closeUpdatePopup() {
    this.setState({ updatePopupOpen: false });
  }

  handleSearch(e) {
    this.setState({ searchString: e.target.value.toLowerCase() }, () => {
      this.changeTbody();
    });
  }

  changeTbody() {
    if (this.state.searchString === "") {
      this.setState({ tbody: this.state.tbodyall });
    } else {
      const searchString = this.state.searchString;
      const newTbody = this.state.tbodyall.filter((phone) =>
        phone.name.toLowerCase().match(searchString)
      );
      this.setState({ tbody: newTbody });
    }
  }

  render() {
    onMessageListener()
      .then((payload) => {
        const { title, body } = payload.data;
        toast.info(`${title}` + "\n" + `${body}`);
      })
      .catch((err) => {
        toast.error(JSON.stringify(err));
      });

    return (
      <div className="content">
        <Popup
          open={this.state.addPopupOpen}
          closeOnDocumentClick
          onClose={this.closeAddPopup}
        >
          <div className="modal">
            <a className="close" onClick={this.closeAddPopup} />
            <h2>Add new Phone</h2>
            <MyForm
              phone={{
                id: 0,
                name: "",
                description: "",
                price: "",
              }}
              myMethod={this.addNewPhone}
            />
          </div>
        </Popup>
        <Popup
          open={this.state.updatePopupOpen}
          closeOnDocumentClick
          onClose={this.closeUpdatePopup}
        >
          <div className="modal">
            <h2>Update Phone</h2>
            <a className="close" onClick={this.closeUpdatePopup} />
            <MyForm
              phone={this.state.tbody[this.state.updateIndex]}
              myMethod={this.updatePhone}
            />
          </div>
        </Popup>
        <ToastContainer
          autoClose={2000}
          position="top-center"
          className="toast-container"
          toastClassName="dark-toast"
        />
        <Card tag="card">
          <CardHeader>
            <CardTitle tag="h1">Phones</CardTitle>
          </CardHeader>
          <CardBody>
            <Input
              type="text"
              value={this.state.searchString}
              onChange={this.handleSearch}
              placeholder="Search by name..."
            />
            <Button onClick={() => this.onAddBtnClick()}>Add new</Button>

            <Table responsive hover>
              <Thead className="text-primary">
                <tr>
                  {this.state.thead.map((prop, key) => {
                    return (
                      <th key={key} id="header">
                        {prop}
                      </th>
                    );
                  })}
                </tr>
              </Thead>
              <Tbody>
                {this.state.tbody.map((prop, key) => {
                  return (
                    <tr key={key}>
                      <td>{prop._id}</td>
                      <td>{prop.name}</td>
                      <td>{prop.description}</td>
                      <td>{prop.price} Dollar</td>
                      <td>
                        <Button onClick={() => this.onUpdateBtnClick(key)}>
                          Update
                        </Button>
                        <Button onClick={() => this.onDeleteBtnClick(key)}>
                          Delete
                        </Button>
                        <Button onClick={() => this.onPriceInRonBtnClick(key)}>
                          Price in RON
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              </Tbody>
            </Table>
          </CardBody>
        </Card>
      </div>
    );
  }
}

class MyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      description: "",
      price: "",
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.setState({
      id: this.props.phone._id,
      name: this.props.phone.name,
      description: this.props.phone.description,
      price: this.props.phone.price,
    });
  }

  handleSubmit(event) {
    const phone = {
      id: this.state.id,
      name: this.state.name,
      description: this.state.description,
      price: this.state.price,
    };
    this.props.myMethod(phone);
    event.preventDefault();
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  render() {
    return (
      <div>
        <Form className="formdata" onSubmit={this.handleSubmit}>
          <FormGroup>
            <Input
              placeholder="Name"
              name="name"
              value={this.state.name}
              onChange={this.handleInputChange}
            />
            <br />
          </FormGroup>
          <FormGroup>
            <Input
              placeholder="Description"
              name="description"
              value={this.state.description}
              onChange={this.handleInputChange}
            />
            <br />
          </FormGroup>
          <FormGroup>
            <Input
              placeholder="Price"
              name="price"
              value={this.state.price}
              type="number"
              onChange={this.handleInputChange}
            />
            <br />
          </FormGroup>
          <Input type="submit" value="Submit" className="form-input" />
        </Form>
      </div>
    );
  }
}

export default PhonesPage;
