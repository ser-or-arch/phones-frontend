import firebase from "firebase/app";
import "firebase/messaging";

const firebaseConfig = {
  apiKey: "AIzaSyCnXNqLj5_KUzoitPy9WK1yopyU0kfVCbU",
  authDomain: "soa-phones.firebaseapp.com",
  projectId: "soa-phones",
  storageBucket: "soa-phones.appspot.com",
  messagingSenderId: "912540496355",
  appId: "1:912540496355:web:50b3db36cab6adc71df0d6",
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

export const requestFirebaseNotificationPermission = () =>
  new Promise((resolve, reject) => {
    messaging
      .requestPermission()
      .then(() => messaging.getToken())
      .then((firebaseToken) => {
        resolve(firebaseToken);
      })
      .catch((err) => {
        reject(err);
      });
  });

export const onMessageListener = () =>
  new Promise((resolve) => {
    messaging.onMessage((payload) => {
      resolve(payload);
    });
  });
