import DEFAULT_URL from "../config/configurations";

export function priceInRon(phone) {
  return fetch(`${DEFAULT_URL}/currency/${phone.price}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: window.sessionStorage.getItem("token"),
    },
  })
    .then(async (response) => {
      if (response.ok) {
        var res = await response.json();
        return res;
      } else {
        alert("Error!");
      }
    })
    .catch((error) => console.warn(error));
}
