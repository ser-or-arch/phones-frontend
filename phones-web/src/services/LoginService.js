import DEFAULT_URL from "../config/configurations";

export function login(username, password, page) {
  return fetch(`${DEFAULT_URL}/auth/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  })
    .then(async (response) => {
      if (response.ok) {
        var res = await response.json();
        window.sessionStorage.setItem("token", "Bearer " + res.access_token);
        window.sessionStorage.setItem("state", "logged");
        window.sessionStorage.setItem("username", username);
        page.forceUpdate();
      } else {
        alert("Authentication failed!");
      }
    })
    .catch((error) => console.warn(error));
}

export function saveToken(username, token) {
  return fetch(`${DEFAULT_URL}/setToken`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: window.sessionStorage.getItem("token"),
    },
    body: JSON.stringify({
      username: username,
      token: token,
    }),
  })
    .then(async (response) => {
      if (response.ok) {
        console.log("Token saved!");
      } else {
        console.log("Error!");
      }
    })
    .catch((error) => console.warn(error));
}
