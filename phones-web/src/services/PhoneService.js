import DEFAULT_URL from "../config/configurations";

export function getPhones() {
  return fetch(`${DEFAULT_URL}/phone`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: window.sessionStorage.getItem("token"),
    },
  })
    .then(async (response) => {
      if (response.ok) {
        var res = await response.json();
        return res;
      } else {
        return null;
      }
    })
    .catch((error) => console.warn(error));
}

export function updatePhone(phone) {
  return fetch(`${DEFAULT_URL}/phone/${phone.id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: window.sessionStorage.getItem("token"),
    },
    body: JSON.stringify(phone),
  })
    .then(async (response) => {
      if (response.ok) {
        var res = await response.json();
        return res;
      } else {
        return null;
      }
    })
    .catch((error) => console.warn(error));
}

export function deletePhone(phone) {
  return fetch(`${DEFAULT_URL}/phone/${phone._id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: window.sessionStorage.getItem("token"),
    },
  })
    .then(async (response) => {
      if (response.ok) {
        var res = await response.json();
        return res;
      } else {
        return null;
      }
    })
    .catch((error) => console.warn(error));
}

export function insertPhone(phone) {
  return fetch(`${DEFAULT_URL}/phone`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: window.sessionStorage.getItem("token"),
    },
    body: JSON.stringify(phone),
  })
    .then(async (response) => {
      if (response.ok) {
        var res = await response.json();
        return res;
      } else {
        return null;
      }
    })
    .catch((error) => console.warn(error));
}
